# Installation ansible

``` bash
# setup a virtualenv
virtualenv venv
source venv/bin/activate
# install ansible
pip install ansible==2.9.16
```
# Configuration environnement-

- create ```inventory``` directory and create a file conf.txt

```
[proxy]
p ansible_port=22 ansible_host=192.168.33.10 ansible_user=vagrant ansible_ssh_private_key_file=/Users/.../.../vagrant/.vagrant/machines/proxy/virtualbox/private_key

[server]
ds ansible_port=22 ansible_host=192.168.33.11 ansible_user=vagrant ansible_ssh_private_key_file=/Users/.../.../vagrant/.vagrant/machines/server/virtualbox/private_key
```

# Useful commands

- To set passphrass:
```ssh-add ~/.ssh/my_pubkey```

- To pass a root password
```ansible-playbook -i inventory/conf.txt --ask-become-pass --vault-password-file file playbook.yml```

- To encrypt ansible value with ansible-vault
```ansible-vault encrypt_string --vault-password-file CHEMIN_VERS_CLE_VAULT valeur_a_chiffrer```
